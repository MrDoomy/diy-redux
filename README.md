# DIY: Redux

## State Management - Home Made

State Management is a reactive programming approach. In the React ecosystem, this concept is associated to Redux...  
But, you can do it yourself!

## Process

Repository:

```
git clone https://gitlab.com/dmnchzl/diy-redux.git
```

Install:

```
yarn install
```

Dev:

```
yarn start
```

Build:

```
yarn build
```

## License

```
"THE BEER-WARE LICENSE" (Revision 42):
<phk@FreeBSD.ORG> wrote this file. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return. Damien Chazoule
```
