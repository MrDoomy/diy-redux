import { useContext } from 'react';
import Elevation from '../components/Elevation';
import LoremIpsum from '../components/LoremIpsum';
import IdentityForm from './IdentityForm';
import ProForm from './ProForm';
import FantasyForm from './FantasyForm';
import ContactForm from './ContactForm';
import { store } from '../StateProvider';

const getClass = idx => {
  switch (idx) {
    case 0:
      return 'is-blue';
    case 1:
      return 'is-red';
    case 2:
      return 'is-yellow';
    case 3:
      return 'is-green';
    default:
      return '';
  }
};

function StateConsumer() {
  const { state } = useContext(store);

  return (
    <section className={`container ${getClass(state.currentIdx)}`}>
      <div className="columns">
        <Elevation deskSize={8} tabSize={10} isReversed={state.currentIdx % 2 !== 0}>
          <div className="column is-left">
            {state.currentIdx % 2 === 0 && <LoremIpsum />}
            {state.currentIdx === 1 && <ProForm title="Professional" description="Let's talk about your work..." />}
            {state.currentIdx === 3 && (
              <ContactForm title="Contact" description="Finally, leave your contact details..." />
            )}
          </div>

          <div className="column is-right">
            {state.currentIdx % 2 !== 0 && <LoremIpsum />}
            {state.currentIdx === 0 && <IdentityForm title="Identity" description="Who are you?" />}
            {state.currentIdx === 2 && (
              <FantasyForm title="Fantasy" description="In a parallel universe, who would you be?" />
            )}
          </div>
        </Elevation>
      </div>
    </section>
  );
}

export default StateConsumer;
