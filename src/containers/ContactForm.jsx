import { string } from 'prop-types';
import Form from '../components/Form';
import InputField from '../components/InputField';
import useIndex from '../hooks/useIndex';
import useField from '../hooks/useField';

function ContactForm(props) {
  const { decrementIndex } = useIndex();
  const [email, setEmail] = useField('email');

  return (
    <Form {...props} handlePrevious={decrementIndex}>
      <InputField
        label="Email"
        type="email"
        placeholder="rick.sanchez@pm.me"
        defaultValue={email}
        handleChange={e => setEmail(e.target.value)}
      />
    </Form>
  );
}

ContactForm.propTypes = {
  title: string,
  description: string
};

export default ContactForm;
