import { string } from 'prop-types';
import Form from '../components/Form';
import SelectField from '../components/SelectField';
import InputField from '../components/InputField';
import useIndex from '../hooks/useIndex';
import useField from '../hooks/useField';

function ProForm(props) {
  const { incrementIndex, decrementIndex } = useIndex();
  const [stack, setStack] = useField('stack');
  const [language, setLanguage] = useField('language');
  const [other, setOther] = useField('other');

  return (
    <Form {...props} handlePrevious={decrementIndex} handleNext={incrementIndex} isDisabled={!stack || !language}>
      <SelectField
        label="Stack"
        options={['Front', 'Back', 'FullStack']}
        defaultValue={stack}
        isRequired
        handleChange={e => setStack(e.target.value)}
      />
      <SelectField
        label="Language"
        options={['JavaScript', 'Python', 'Java', 'PHP', 'Other']}
        defaultValue={language}
        isRequired
        handleChange={e => setLanguage(e.target.value)}
      />
      <InputField
        label="Other"
        placeholder="Go, Rust..."
        defaultValue={other}
        handleChange={e => setOther(e.target.value)}
      />
    </Form>
  );
}

ProForm.propTypes = {
  title: string,
  description: string
};

export default ProForm;
