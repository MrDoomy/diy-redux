import { useContext } from 'react';
import { store, ACTIONS } from '../StateProvider';

export default function useIndex() {
  const {
    state: { currentIdx },
    dispatch
  } = useContext(store);

  const incrementIndex = () => dispatch({ type: ACTIONS.INCREMENT_INDEX });
  const decrementIndex = () => dispatch({ type: ACTIONS.DECREMENT_INDEX });

  return { currentIdx, incrementIndex, decrementIndex };
}
