import { useContext } from 'react';
import { store, ACTIONS } from '../StateProvider';

export default function useField(key) {
  const {
    state: { fieldValues },
    dispatch
  } = useContext(store);

  const setField = key => value => dispatch({ type: ACTIONS.SET_FIELD, payload: { key, value } });

  return [fieldValues[key] || '', setField(key)];
}
