import { string, arrayOf, bool, func } from 'prop-types';
import Field from './Field';

function SelectField({ label, options, defaultValue, isRequired, handleChange }) {
  return (
    <Field label={label} isRequired={isRequired}>
      <div className="select is-fullwidth">
        <select onChange={handleChange} defaultValue={defaultValue}>
          <option value="" disabled>
            Unknown
          </option>
          {options.map((option, idx) => (
            <option key={idx} value={option}>
              {option}
            </option>
          ))}
        </select>
      </div>
    </Field>
  );
}

SelectField.defaultProps = {
  options: [],
  isRequired: false
};

SelectField.propTypes = {
  label: string,
  options: arrayOf(string),
  defaultValue: string,
  isRequired: bool,
  handleChange: func
};

export default SelectField;
