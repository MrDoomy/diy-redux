function LoremIpsum() {
  return (
    <>
      <h1 className="title is-2">State Management</h1>
      <h2 className="subtitle colored is-3">Home Made</h2>
      <p>
        State Management is a reactive programming approach. In the React ecosystem, this concept is associated to
        Redux...
        <br />
        But, you can do it yourself!
      </p>
    </>
  );
}

export default LoremIpsum;
