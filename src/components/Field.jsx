import { string, bool, node } from 'prop-types';

function Field({ label, isRequired, children }) {
  return (
    <div className="field">
      <div className="label">
        {label}
        {isRequired ? ' *' : ''}
      </div>
      <div className="control">{children}</div>
    </div>
  );
}

Field.defaultProps = {
  isRequired: false
};

Field.propTypes = {
  label: string,
  isRequired: bool,
  children: node
};

export default Field;
