import { string, func, bool, node } from 'prop-types';

function Form({ title, description, handlePrevious, handleNext, isDisabled, children }) {
  return (
    <div className="with-form">
      <h3 className="title is-4">{title}</h3>
      <p className="description">{description}</p>
      <form
        onSubmit={e => {
          e.preventDefault();
          handleNext && handleNext();
        }}>
        {children}
        <div className="columns buttons">
          {handlePrevious && (
            <div className="column">
              <button className="button is-primary is-fullwidth" type="button" onClick={handlePrevious}>
                Previous
              </button>
            </div>
          )}

          {handleNext && (
            <div className="column">
              <button className="button is-primary is-fullwidth" type="submit" disabled={isDisabled}>
                Next
              </button>
            </div>
          )}
        </div>
      </form>
    </div>
  );
}

Form.defaultProps = {
  isDisabled: false
};

Form.propTypes = {
  title: string,
  description: string,
  handlePrevious: func,
  handleNext: func,
  isDisabled: bool,
  chidlren: node
};

export default Form;
